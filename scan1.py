#Author: Jumani Blango

import socket
import sys
import struct
import threading
from queue import Queue
import os


output_lock = threading.Lock() 


print('')
print('')

def scanSocketPorts(port,host):
    socket.setdefaulttimeout(5) 
    try:
        #get a stream socket
        sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  

        #connect to a host on a specific port 
        conn2 = sock2.connect((host, port))

        #close socket
        sock2.close()
        
        report =  str(port) + '\t\t\t Open \t\t\t  '  
        
        with output_lock:
            print(report)
       
           
    except KeyboardInterrupt:
        print ('User interrupted')
        sys.exit()

    except socket.gaierror:
        print ('Hostname Exiting')
        return 2

    except socket.error:
        ##print ('Socket error connect to server')
        pass

def threadFunc(queue,host):
    while True:
        port = queue.get()
        scanSocketPorts(port,host)
        queue.task_done()



def main():
    hostAddress = "127.0.0.1"
    port1 = "1"
    port2 = "1025"

    if len(sys.argv) != 4:
        print(">scan1.py IPADDRESS StartPortNumber EndPortNumber")
        sys.exit("Exiting...")
    
    hostAddress = sys.argv[1]
    port1 = int(sys.argv[2])
    port2 = int(sys.argv[3])
    p1 = int(port1)
    p2 = int(port2)
    if (p1 <= 0):
        print ('Invalid starting port :',port1)
        sys.exit("Exiting...")

  
    if (p2 <= 0):
        print ('Invalid ending port :',port2)
        sys.exit("Exiting...")

    if (p1 > p2):
        print ('Staring port should not be greater than ending port')
        sys.exit("Exiting...")

        
    print ("*" * 80)
    print ("Please wait, scanning remote host: ", hostAddress, ' range(',port1,',',port2,')')
    print ("*" * 80)

    #create a queue
    queue = Queue()

    print ('\n Port \t\t\tStatus\t\t\t Protocol')
    print ('------\t\t\t------\t\t\t -------')

    # add ports to queue
    for port in range(p1,p2):
        queue.put(port)

    #spawn threads to scan the ports 
    for threadCount in range(100):
        thread1 = threading.Thread(target = threadFunc, args = (queue,hostAddress))
        thread1.daemon = True
        thread1.start()
       

    queue.join()
    
if __name__ == "__main__":
    main()
